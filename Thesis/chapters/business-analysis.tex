\section{Introduction}

\newcommand{\backgroundTitle}{Background}
\subsection{\backgroundTitle{}}
There exist many different programming languages. Each of them is created for some particular purpose.
Each works in a different manner and has a distinct syntax. Some languages were created decades ago, some are only couple of years old.
Every programming language has some unique set of advantages and disadvantages. With the passage of time newer and newer languages
are created. When designing such a new language, its designers always try to avoid mistakes of their predecessors. This is essential
because once language is created and programmers start using it, it is much more difficult to introduce improvements and fixes to
the language itself. There are two most important reasons to that fact. First of all, when language is widely used, there is multiple
projects created which are using this language. In most cases this means that authors can only extend existing syntax but still they 
have to make sure that current contract will not be broken by changes to be introduced. It means, all projects that could have 
potentially been created using the old syntax should work properly after introducing updates to the language. In practice it is
very strict requirement and significantly limits amount and range of potential improvements. Certainly, we can claim that next
patch of changes will be brand new version of the language and old projects will have to be adjusted in order to use the new
features of the language. However, this is barely different from just creating a new language. Second issue why it is so hard
to introduce changes to existing and widely used language is a big community. Often it turns out that it is very difficult to find
proper development path for a language. Such a path that will satisfy majority of the society related to the language. This causes
big delays in the evolution of some languages.

\newcommand{\problemTitle}{The Problem}
\subsection{\problemTitle{}}
The fact that it is so difficult to introduce changes to the existing language is very disturbing, because as we know there is a big 
amount of very popular programming languages that are quite old already but cannot be updated. This limitation exists mainly due to 
reasons mentioned in previous section but is not restricted to them. One of languages that is affected by those problems is definitely 
Java. Currently, its most popular version is 1.8. Last years a new version of Java has been brought to the market every couple of years 
(it has now increased to frequency of 2 releases per year). However those updates almost always require adjusting your project, which
can be very labor-intensive for bigger projects. Moreover, those changes are often rather poor and minimalistic. Hence, syntax of Java
still remains far different from the syntaxes of the most modern languages such as Kotlin or TypeScript.
In case of Java switching to other language is significantly easier than in general, because there are modern alternatives on the market, that run on JVM and from the point of view of runtime environment they are indistinguishable from Java.
Languages such as Kotlin or Scala can be seamlessly mixed with Java code, so we can upgrade the language and preserve all the good things that we are used to - frameworks, libraries, build tools, deployment environments, application servers etc.
But for companies, even in case of Java switching to another language would most often be too radical.
It would require developers to learn new language and more importantly - hiring a Kotlin developer is incomparably harder than hiring a Java developer.
Since cure would be worse than the disease, there is no other choice than sticking to Java and hoping for improvements.

\newcommand{\desiredSolutionTitle}{Desired Solution}
\subsection{\desiredSolutionTitle{}}
We are interested in a pluggable solution that would be somewhere in between - less radical than switching to another language, but at the same time enabling most valuable features.
In other words, we have to bring missing features to the language, preserving existing syntax.
Solution may have certain limitations, but it should address most typical boilerplate code scenarios and deliver most useful features available in modern languages.
Apart from that, there should be possibility to add custom language features using helper library.
Solution would ideally be available as a framework. 
Using it should require only adding a dependency, just like using any other framework or library.


\newcommand{\inspirationTitle}{Existing Solution}
\subsection{\inspirationTitle{}}
This project is strongly inspired by already existing solution called Project Lombok \cite{ProjectLombok}. Lombok declares a set 
of annotations that can be used for code manipulation during compilation. One of the great features from Lombok is type
inference using \textbf{var} keyword instead of actual type for local variables (exactly like in C\#). In fact var is just an 
annotation replaced with inferred type name during annotation processing.
Lombok achieves its functionality in the same way as we are going to. They prepared a library performing code alterations from the level of annotation processor. Unfortunately their library is internal and the only way to extend it is forking their project. Lombok
developers admit the fact that they are coding against the compiler API, but there is no other way to achieve the same result.

\newcommand{\documentStructureTitle}{Document Structure}
\subsection{\documentStructureTitle{}}
First chapter (Business Analysis) starts with the introduction followed by first detailed example of a problem that we deal with in Case Study - Section \ref{CaseStudy}. 
Then in sections \ref{ExecutiveSummary} - \ref{NonFunctionalRequirements} we define our product - Project \projectName{}. 
In Section \ref{RiskAnalysis} project related risks along with contingency plans are discovered and analyzed. 
Business Analysis ends with Work Schedule - Section \ref{WorkSchedule}.

Second chapter is a technical analysis, which consists of three sections. 
First devoted entirely to javac compiler analysis with particular attention to elements that we interfere with.
Second describes architecture of project \projectName{}, testing possibilities and API that we expose in order to make the framework extensible.
Last section introduces basic concepts related to IntelliJ Platform plugins, describes our solution and testing methodology.

Third chapter, Post-development Analysis, describes all features that we implemented - their behavior and method of operation. This analysis contains also information about most interesting implementation details from entire framework, distribution model for framework and plugin, unexpected changes and unexpected problems.

Forth chapter contains conclusions, future perspectives for the project and distribution of work between team members.

Appendix A contains full user manual, providing all necessary information for a person using our framework.
Appendix B contains Glossary.
Appendix C contains List of Abbreviations.
In Appendix D we put diagrams depicting structure of AST tree generated by \projectName{}, which may be important for anyone extending the framework with new features.


\newcommand{\caseStudyTitle}{Case Study}
\section{\caseStudyTitle{}} \label{CaseStudy}
In most popular enterprise Java frameworks (e.g. Spring Framework) dependency injection is one of the core concepts supporting
development of complicated applications. Thanks to modularity of applied pattern, dependency injection based systems are
relatively easy to maintain and extend. However Java was not focused mainly on this aspect of programming, so there is still
space for improvements. Dependency injection is based on a container supplied by inverse of control framework. Java classes can
be registered as so called beans - beans are instantiated and managed by the framework, not by developer using the framework.
Any bean can have fields marked as dependencies to be injected by the framework during instantiation. For example in Spring
there is \lstinline{@Autowired} annotation for marking field or constructor as a place to inject other beans automatically when
instance is created. This creates a graph of dependencies between objects in Spring context. There are two types of injection -
field injection (when field is marked \lstinline{@Autowired}) and constructor injection (when constructor has
\lstinline{@Autowired} annotation).\\

\subsection{Challenge}
Field injection is more convenient for the programmer, because it requires only
adding a field and putting one annotation on it. It requires less code compared to
constructor injection, where it is necessary to create a field, add parameter to the
constructor and assign bean to a field. Constructor must also be marked \lstinline{@Autowired}.
Unfortunately field injection has a few drawbacks - it may lead to cyclic dependencies between beans and this way we cannot declare injected fields final. Making fields final would protect them from overriding.
Because of that constructor injection is the recommended way of injecting beans.
Now the question is, can we develop a solution that preserves convenience of field injection but does not allow dependency cycles? The goal is to minimize the boilerplate associated with constructor injection.

\subsection{Solution}
Our solution brings a feature from TypeScript language to Java. 
In TypeScript we can add access modifier before constructor parameter, which results in creating a field with the same type and
name in the class. Value from such constructor parameter is assigned to the auto-generated field. So in TypeScript, this:
\begin{lstlisting}[language=TypeScript]
class Car {
constructor(public brand: string)
}
\end{lstlisting}
Is the same as this:
\begin{lstlisting}[language=TypeScript]
class Car {
brand: string;

constructor(brand: string) {
this.brand = brand;
}
}
\end{lstlisting}

Of course in Java putting access modifier before a method parameter is illegal and will not even be accepted by parser.
Instead of modifier we can put an annotation before parameter and then during annotation processing perform necessary code generation for all marked parameters.
We will supply four annotations: 
\lstinline{@Private}, 
\lstinline{@PackagePrivate}, 
\lstinline{@Protected}, 
\lstinline{@Public} 
to indicate demanded access level.

\subsection{Benefits}
Our solution makes constructor injection much more user friendly, which is expected to boost proper programming practices with minimal effort.
Resulting code will be cleaner than in existing solutions, because we minimize the amount of code implementing the functionality.
Furthermore all objects that our bean is dependent on will be listed in the constructor.
To add dependency it is enough to modify constructor parameters list and apply adequate annotation. 
Removing dependency is as easy as removing a parameter. 
\projectName{} deals with the whole boilerplate that user willing to apply proposed pattern in vanilla Java would have to repeat manually for every injected bean:
adding field to the class, adding final modifier, assigning value to the field.
Only constructor parameter remains, but as we see, it is enough.
This feature is very complementary to dependency injection frameworks existing in Java and mainly focused on them. Thanks to inverse of control constructor with annotated fields will be called by the framework, not by the programmer. The framework will supply parameters for which we "ask" by inserting constructor parameter.

\subsection{Result}
Sample code below depicts how this feature is supposed to work with Spring framework.
\\
\\
Example with our annotations:
\begin{lstlisting}[language=Java]
class Example {

@Autowired
public Example(@Private SomeService someService, @Protected SomeOtherService otherService) {

}

}
\end{lstlisting}

Vanilla Java example:
\begin{lstlisting}[language=Java]
class Example {

private final SomeService someService;
protected final SomeOtherService otherService;

@Autowired
public Example(SomeService someService, SomeOtherService otherService) {
this.someService = someService;
this.otherService = otherService;
}
}
\end{lstlisting}


\section{Executive Summary}\label{ExecutiveSummary}
\newcommand{\descriptionOfProductTitle}{Description of the Product}
\subsection{\descriptionOfProductTitle{}}
\projectName{} is a tool that aims at facilitating Java programmers work. It does it by allowing a programmer
to create some boilerplate pieces of code with a very little effort in an automatic manner. There is a set of
annotations defined by the library. Each of them allows for creation or modification of a code. Whenever programmer
decides he or she needs one of the functionalities supplied with a framework, one can use annotation instead of
manual creation of required code. Later, during project compilation, all the code is created/modified automatically
according to instructions specified by a programmer with all the annotations that he or she used. Functionality of automatic
code modification is not supported by IDEs, so we also prepare a plugin for the most popular Java IDE called
IntelliJ IDEA \cite{IntellijIdea} produced by JetBrains company. This plugin will ensure that no errors caused
by missing or unmodified-yet code are displayed.

\newcommand{\howItCanBeUsedTitle}{How It Can Be Used?}
\subsection{\howItCanBeUsedTitle{}}
Developer that is going to use \projectName{} framework should become familiar with the list of defined annotations.
Each annotation is identified with its unique name. For each annotation there exists a detailed description of 
how it works and when it can be used. There is one more thing related to the annotation - its target. Some annotations
can be used only on classes, some other on property or method declarations. This is something that is also always
specified within annotation documentation. If project \projectName{} is properly added to the user's project's classpath,
it will be automatically used during project build, so there is nothing that programmer has to do except of applying
selected annotations in one's source code.

\newcommand{\howItWorksTitle}{How It Works?}
\subsection{\howItWorksTitle{}}
For each annotation provided by \projectName{} framework as a language extension, there is a processor created. This unit is
called by javac compiler during annotation processing stage. Every Java compiler supporting 1.6 and higher Java edition includes
annotation processing as compilation stage following parsing .java files to representing them Java objects - so called 
Abstract Syntax Trees (AST). Structure of AST according to javac specification is implemented as Java classes in package 
\lstinline{javax.lang.model} of the compiler. At the time of annotation processing AST provides partial information about Java types 
being compiled. Also, since files are only parsed and inserted to the AST structure, only lexical correctness (determined by javac 
parser) of individual files is checked and any other errors are checked in subsequent stages. This is crucial for \projectName{} 
implementation, because code with extensions may not be a valid Java source file but still compile due to transformations on AST tree 
made before the compiler checks correctness and it will work as long as the code is accepted by the parser.\\
Annotation processing API, according to its documentation works as follows:
\begin{enumerate}
	\item \label{processing} Take a list of compilation units as input.
	\item Find all annotations used in compilation units.
	\item For each annotation call processors registered for processing this annotation.
	\item If new .java files were generated in this round and no errors occurred, parse them to compilation units and pass them
			as input to \ref{processing}..
\end{enumerate}

Official API does not allow modifications in parsed compilation units, but knowledge of internal structure of the compiler can be used to obtain a reference to compilation unit by annotation processor. Then we can perform our transformations.

Additionally, we create an IntelliJ IDEA \cite{IntellijIdea} IDE plugin in order to suppress error messages that
are caused by code structured incorrectly according to Java specification but properly if \projectName{} framework is
used (for example usage of methods that are not declared in a code but will be generated automatically).


\newcommand{\functionalRequirementsTitle}{Functional Requirements}
\section{\functionalRequirementsTitle{}}
\begin{itemize}
	\item Allow for extension of Java language with a set of predefined annotations.
	\item Expose AST transformation API allowing user to implement custom extensions in a manner similar to extensions provided
			by \projectName{}.
	\item Errors are checked and logged at compilation time. 
	 Our extensions lead to errors in the same way as any other language features if used incorrectly.
	 Compile errors from Java compiler after AST transformations may not be precise and informative enough to signal misuse of annotations as the actual source of error.
	 Constraints of our annotations usage cannot always be expressed as restrictions on annotation target. 
	 We must imperatively check if annotation is used properly and in case of misuse break the compilation with an error displayed
	 to the user and a message describing which particular constraint was not satisfied.
	\item Extensive javadoc documentation of every annotation that is part of API.\\
	 Our documentation consists for each annotation of the following:
		\begin{itemize}
			\item Detailed description of how the annotation influences resulting code.
			\item Constraints on use of the annotation.
			\item List and description of all parameters of the annotation.
			\item Usage sample exemplifying the feature, along with example of achieving the same functionality in vanilla Java.
		\end{itemize}
	\item A plugin for IntelliJ IDEA \cite{IntellijIdea} exists so that there are not any unnecessary errors displayed.
	\item A plugin will make IDE suggestions work properly.
	\item Examples o possible (types of) annotations that can be implemented by \projectName{} framework:
	 \begin{itemize}
	 	\item \lstinline{@Private}, \lstinline{@PackagePrivate}, \lstinline{@Protected}, \lstinline{@Public} - TypeScript-style field 
	 			generation from constructor parameters. See \ref{CaseStudy} for details.
	 	\item \lstinline{@Default} - for default primitive parameters in Java methods.
	 	\item \lstinline{@Clone} - for creating a method that calls annotated method, processes returned value and returns processed 
	 			value.
	 	\item \lstinline{@ConstructorArg} - all annotated fields will be used as parameters to generated constructor. Constructor 
	 			will assign values to those fields.
	 	\item \lstinline{@ExtensionMethod} - Implementation of extension method mechanism. This feature is available for
	 			example in C\#.
	 	\item \lstinline{@Yield} - annotated method will be able to return multiple values.
	 	Method will return iterator with all values available in the same order as they were originally returned.
	 \end{itemize}
	\item Deployment document supplied.
\end{itemize}


\newcommand{\nonFunctionalRequirementsTitle}{Non-functional Requirements}
\section{\nonFunctionalRequirementsTitle{}}\label{NonFunctionalRequirements}
\begin{itemize}
	\item \projectName{} works correctly with javac compiler (we do not support other compilers).
	\item Framework works correctly with Java 1.8.
	\item Classes are processed automatically during compilation.
	\item The only configuration required is to add \projectName{} project's .jar file to the classpath.
	\item Each annotation name is short, easy to understand, and suggest what it does.
	\item Performance - build time is not increased significantly when \projectName{} is used.
	\item Supportability - README file is added to each project (framework and plugin) and it contains basic information
			about the project and its usage.
	\item There is a HTML file attached to the project that contains page with documentation and examples.
	\item Distribution - sources available in Bitbucket service, project added to the Apache Maven Repository, IntelliJ IDEA
			plugin available in JetBrains Plugins Repository.
	\item Product \projectName{} and IntelliJ IDEA plugin are both distributed under the MIT License as defined in
			\url{https://opensource.org/licenses/MIT} and also in LICENSE file attached to each of the projects.
\end{itemize}

\newcommand{\riskAnalysisAndManagementTitle}{Risk Analysis and Management}
\section{\riskAnalysisAndManagementTitle{}}\label{RiskAnalysis}
\noindent Here we present a risk analysis matrix, where we depict potential risks that may be encountered while
working on the project. Later - below the table - for each threat, we introduce its description, our strategy to
avoid it and also contingency plan for the sake of failure.\\

\begin{samepage}
\subsection{Risk Matrix}
\noindent \resizebox{\textwidth}{!}{
\begin{tabular}{|l|c|l|l|l|}
		\hline
		\multicolumn{2}{|c|}{} & \multicolumn{3}{c|}{\textbf{Severity}}\\ \cline{3-5}
		
		\multicolumn{2}{|c|}{} & \makecell{\textbf{Minor}} & \makecell{\textbf{Moderate}} 
		 & \makecell{\textbf{Significant}} \\ \hline
		
		\multirow{4}{*}[-6ex]{\rotatebox{90}{\textbf{Likelihood}}} & \textbf{Likely} 
		 & \makecell[l]{} & \makecell[l]{} & \makecell[l]{} \\ \cline{2-5}
		
		& \textbf{Possible} & \makecell[l]{}  & \makecell[l]{- Underestimation of some tasks\\
			- Problem with adding \projectName{}\\ to Maven repository}
	     & \makecell[l]{- Problem with adding plugin\\ to JetBrains Plugin Repository} \\ \cline{2-5}
		
		& \textbf{Unlikely} & \makecell[l]{}  & \makecell[l]{- Cooperation problems\\- Unexpected events like
			sickness etc.\\- Equipment failure} & \makecell[l]{- Underestimation of some\\ important tasks\\
			- Lack of knowledge\\- Lack of skills}\\
		\hline
	\end{tabular}
}\\
\end{samepage}

\subsection{Underestimation of Tasks}
\subsubsection{Description}
Underestimation my be caused by multiple factors. Most common of them are:
\begin{itemize}
	\item too little attention during estimation
	\item not enough knowledge
	\item lack of experience
\end{itemize}
\subsubsection{Strategy to Avoid Issues}
In order to reduce risk of underestimation we put a lot of attention when planning what we are going to do and when.
We are not aware of everything that concerns our project, so we make efforts to reduce our unknowing. Before planning
and estimating everything have we spend some time reading documentations and sources of similar projects. Thanks to that, 
we can estimate tasks' labor consumption more accurately. Later on, we are going to iteratively check our assumptions and
in case we discover some new information we will adjust estimations appropriately.
\subsubsection{Contingency Plan}
If we discover something was underestimated, we will estimate it again using a newly achieved knowledge.
Then we should think if now we can still finish the task by the expected due date. If not, we will look at
all tasks' priorities and their estimations and we may have to discard one of tasks for the sake of overall success
of the project.

\subsection{Cooperation Problems}
\subsubsection{Description}
Cooperation problems might occur in any team. They may be caused by many various reasons. It can be for instance bad
communication leading to misunderstanding or a difference in views on some topic.
\subsubsection{Strategy to Avoid Issues}
We decided to work together because we know each other for many years. We were cooperating on the other projects in past
and we know what to expect. We both like Java which is main topic of the thesis. This makes us both understand the project
better and keeps us motivated.
\subsubsection{Contingency Plan}
In case of problems in this field we have to be very careful. Issues of this kind might be very difficult to solve. First
of all, we both should try to listen and understand other party's rights and opinions. If we still cannot find a compromise,
we can ask our project's supervisor for being a mediator.

\subsection{Lack of Knowledge or Skills}
\subsubsection{Description}
During development of the project we may discover that some work has to be done, that requires more skills or knowledge than
we expected. This may force us to spend more time on studying documentation and other external sources to gain missing
knowledge, which in turn may lead to delays.
\subsubsection{Strategy to Avoid Issues}
In order to reduce threat related to discovery of missing knowledge we start work on the project as soon as possible. 
We try to gain as much expertise as it is possible as fast as possible, in order to reduce uncertainty.
Our work schedule takes into account possibility of small delays, giving us some additional time.
We also try to share knowledge about project between both team members so that they can easily help each other and take over the
responsibilities of the second person in case of emergency.
\subsubsection{Contingency Plan}
The only possible and right option is to simply spend more time working
on the project, doing proper researches and learning. It is important that we have some spare time planned for
such unpleasant surprises.

\subsection{Adding Project to External Repository}
\subsubsection{Description}
Adding project to external repository is the only place where we rely on the third parties which makes it something definitely
worth to consider in terms of the risks related to it. Delays or other problems might appear due to some unexpected requirements
of the system's administrator that we are not able to fulfill or are simply difficult or very time consuming. Second potential
threat is long time of expectancy for the external distribution platform owner to accept our package. This risk is especially
harmful for the plugin because external repository is the only right method of distribution of IntelliJ IDEA plugin.
\subsubsection{Strategy to Avoid Issues}
We read all the requirements of the external repositories and we keep in mind all of them so that they are satisfied in the
end of the development process. We also ensure that we upload our project early enough to be accepted before project's deadline.
\subsubsection{Contingency Plan}
Unfortunately if we are not able to fulfill vendor's requirements or we upload project to the repository too late, there is
not much we can do about it.


\newcommand{\workScheduleTitle}{Work Schedule}
\section{\workScheduleTitle{}}\label{WorkSchedule}
\noindent Methodology: Waterfall\\
Supporting tools: Trello, GIT, Bitbucket\\

\scalebox{0.92}{
\begin{ganttchart}[
	x unit=5pt,
	time slot format=isodate,
	y unit title=0.4cm,
	y unit chart=0.5cm,
	inline,
	title/.append style={draw=none, fill=RoyalBlue!50!black},
	title label font=\sffamily\bfseries\color{white},
	title label node/.append style={below=-1.6ex},
	title left shift=.05,
	title right shift=-.05,
	title height=1,
	bar/.append style={draw=none, fill=OliveGreen!75},
	bar height=.6,
	bar label font=\small\color{black!50},
	group right shift=0,
	group top shift=.6,
	group height=.3,
	group peaks height=.2,
	bar incomplete/.append style={fill=Maroon},
	]{2018-10-01}{2019-01-09}
	
	\gantttitlecalendar{year, month=name} \\
	
	\ganttbar[bar inline label node/.style={right=7mm}]{Decide on methodology}{2018-10-07}{2018-10-14} \\
	\ganttbar[bar inline label node/.style={right=16mm}]{Work schedule}{2018-10-14}{2018-10-31} \\
	\ganttbar[bar inline label node/.style={right=16mm}]{Customer requirements}{2018-10-14}{2018-10-31} \\
	\ganttbar[bar inline label node/.style={right=16mm}]{Risk analysis}{2018-10-14}{2018-10-31} \\

	\ganttbar[bar inline label node/.style={right=13mm}]{Design of framework}{2018-11-1}{2018-11-14} \\
	\ganttbar[bar inline label node/.style={right=13mm}]{Design of plugin}{2018-11-1}{2018-11-13} \\

	\ganttbar[bar inline label node/.style={left=5mm}]{Test Maven Repository}{2018-11-1}{2018-11-7} \\
	\ganttbar[bar inline label node/.style={left=5mm}]{Test JetBrains Plugin Repository}{2018-11-1}{2018-11-7} \\
	
	\ganttgroup{Implementation}{2018-11-14}{2018-12-12}\\
	
	\ganttbar[bar inline label node/.style={left=16mm}]{AST transformation main API}{2018-11-14}{2018-11-28} \\
	\ganttbar[bar inline label node/.style={left=16mm}]{Core part of plugin}{2018-11-13}{2018-11-28} \\
	
	\ganttbar[bar inline label node/.style={left=14mm}]{Implementation of java extensions}{2018-11-28}{2018-12-12} \\

	\\
	\ganttbar[bar inline label node/.style={left=14mm}]{Support extensions in plugin}{2018-11-29}{2018-12-11} \\ 
	\ganttbar[bar inline label node/.style={left=14mm}]{Units tests for each extension}{2018-11-29}{2018-12-12} \\
	
	\ganttbar[bar inline label node/.style={left=9mm}]{Deployment document}{2018-12-13}{2018-12-20} \\
	\ganttbar[bar inline label node/.style={left=24mm}]{HTML documentation}{2018-12-20}{2019-1-9} \\
	\ganttbar[bar inline label node/.style={left=17mm}]{Javadoc documentation}{2018-12-20}{2019-1-1} \\
	
	\ganttbar[bar inline label node/.style={left=26mm}]{Project technical documentation}{2019-1-2}{2019-1-9} \\
	
	\ganttlink{elem2}{elem4}
	\ganttlink{elem2}{elem5}
	
	\ganttlink{elem5}{elem10}
	\ganttlink{elem4}{elem9}
	
	\ganttlink{elem9}{elem11}
	\ganttlink[link type=s-s]{elem11}{elem12}
	\ganttlink[link type=s-s]{elem11}{elem13}
	
	\ganttlink[link/.style={-to, rounded corners=1.5pt}]{elem11}{elem14}
	\ganttlink{elem11}{elem15}
	\ganttlink{elem11}{elem16}
	
	\ganttlink{elem11}{elem17}
	\ganttlink{elem12}{elem17}
\end{ganttchart}
}
\captionof{figure}{Gantt Chart - Schedule}