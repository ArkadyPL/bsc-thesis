\section{Next steps}
\newcommand{\newHandlersTitle}{New Handlers}
\subsection{\newHandlersTitle{}}
With our API, adding new features to the framework is significantly easier than implementing the project from scratch. 
Only understanding of AST tree is required to start developing features.
Many helpful methods are provided by \projectName{} - mostly for modifying AST, creating new nodes and retrieving annotation parameters.
We have some ideas that have not been implemented as part of the project, but can definitely bring a lot of value.
\paragraph{{\textcolor{CarnationPink}{@Default}}}\mbox{} \\
Default parameters can be easily implemented. Feature could be defined as follows:
\begin{itemize}
	\item There is \lstinline|@Default| annotation, with \lstinline|value| parameter.
	\item \lstinline|@Default| can be applied to a method parameter.
	\item \lstinline|value| is not declared as parameter, but uses virtual attribute pattern (see \ref{VirtualAttributes}
							\textbf{Virtual attributes} for details).
	\item \lstinline|value| takes any valid Java expression as parameter. Expression must evaluate to anything assignable to
							parameter on which annotation is placed.
	\item After default parameters in the signature, non-default parameters are illegal.
	\item For every method with default parameters we do the following processing:
		\begin{enumerate}
			\item Take first parameter from the right, having \lstinline|@Default| annotation. Finish processing if none is found. \label{defaultHandlerStart}
			\item Create method with signature identical to original method, except that without the found parameter.
			In new method call original method, passing \lstinline|value| of \lstinline|@Default| as the last parameter. Return result if return type is not void.
			\item Execute point \ref{defaultHandlerStart} for newly generated method.
		\end{enumerate}
\end{itemize}

Of course, this description is not a full implementation plan, but should give an insight into the idea.
Small example, how it could be used:
\begin{lstlisting}[language=Java]
@Override
public void method(@Default(new ArrayList<Object>()) List<Object> someList, @Default(17) Integer number) {
	[...]
}

@Override
public void callingMethod() {
	method();
	method(Collections.singletonList(new Object()));
	method(Collections.singletonList(40), 1000);
}
\end{lstlisting}
If passing any value as parameter of \lstinline|@Default| fails for some reason, annotation can take value of the parameter as String and then handler can use \lstinline|JavacParser| to parse the value, in the same way as string interpolation handler does. It would be a fallback, which is supposed to work without problems, but is not preferable because support in plugin would be much more complicated.

\paragraph{{\textcolor{CarnationPink}{@Yield}}}\mbox{} \\
Many modern programming languages offer a functionality of returning multiple values from a method.
Such method in fact returns some kind of iterator object. 
Typically after method returns a value, its execution is suspended until next value is requested from iterator. 
Next value is obtained by resuming the execution.
This feature is available for example in JavaScript, C\#, Python and Kotlin.
The idea here is to create \lstinline|@Yield| marker annotation, applicable to methods.
Every annotated method would require transforming return type to some iterator type and all return statements to iterator-related logic.
This feature is challenging performance-wise and may be hard to implement efficiently.
Usage example, returning numbers from 0 to 100:
\begin{lstlisting}[language=Java]
@Override
@Yield
public Integer getNumbers() {
	for (int i = 0; i <= 100; i++) {
		return i;
	}
}
\end{lstlisting}

\newcommand{\compilersSupportTitle}{Compilers Support}
\subsection{\compilersSupportTitle{}}
Project \projectName{} supports only \lstinline|javac| compiler. It was a reasonable choice, because we started with very little knowledge, so thinking about support for multiple compilers would significantly slow down our research. Now when the architecture is clarified, it is possible to logically separate our code into two parts:
\begin{enumerate}
	\item Core of the framework, exposing API representing compiler-specific implementation details
	\item Implementation of this API, for javac compiler.
\end{enumerate}
It would allow implementing support for other compilers in the future, for example \lstinline|EJC| compiler.


\newcommand{\javaSupportTitle}{Java Support}
\subsection{\javaSupportTitle{}}
For simplicity, we decided to support only Java 8, because it was the most popular version when project started.
Reliable framework should provide extensive support for a wide range of language versions.
Programmer considering \projectName{} as a framework to use will most likely take into account the fact that version of Java in 
his project would not be upgradable as long as we don't implement support for newer versions.
The biggest challenge in Java above version 8 is definitely Project jigsaw\cite{project_jigsaw}.
It is a major change, because it introduces a modules system. JDK in version 9 drastically changed its structure.
Accessing internal APIs and classes was never a problem, but modules system increases difficulty so that it is now challenging.
Besides, we use a lot of internal APIs, that are naturally subject to change or deletion without notice.
Once we get through modules system, there is still some work necessary to adjust our logic depending on compiler version.
The biggest problem that may happen is related to compiler differences that may lead to compilation failure before annotation 
processing starts. If in newer Java for one of our features compilation fails before annotation processing, we can no longer 
provide the feature in the same form.
\newcommand{\pluginSupportTitle}{Plugin Support}
\subsection{\pluginSupportTitle{}}
Support for our features in the plugin can generally be divided into two groups - required features, that are critical to convenient use of the framework and "nice to have" features, that surely improve the experience, but framework is reasonably usable without them.
Critical aspects are among the others:
\begin{itemize}
	\item Not highlighting any errors, if the code is in fact corrected by \projectName{}
	\item If \projectName{} injects members to definition of any type, IDEA should be aware of these members
\end{itemize}
There are other aspects, with lesser importance. Some of them are fully supported, some only partially and certain functionalities are not implemented.
Examples of further support that could help programmers:
\begin{itemize}
	\item Adding code completion for expressions inside interpolated strings
	\item Error checking for expressions inside interpolated strings
	\item Validation for \lstinline|@Clone| method name and TypeScript Fields names
	\item Refactoring support for members generated with \lstinline|@Clone| and TypeScript Fields
\end{itemize}

\section{\distributionOfWorkTitle{}}\label{distributionOfWork}
\subsection{Framework}
\begin{longtable}{|p{9.4cm} | p{6.5cm}|}
	\hline
	\textbf{Element} & \textbf{Author(s)} \\ \hline
	Java Compilation Process - research & Konrad Gancarz, Arkadiusz Ryszewski \\ \hline
	Java Compilation Process - sequence diagram & Arkadiusz Ryszewski \\ \hline
	Proof of Concept & Konrad Gancarz \\ \hline
	\lstinline|KendalProcessor| class - design \& implementation & Konrad Gancarz \\ \hline
	Registration \& loading of handlers with Java SPI & Konrad Gancarz \\ \hline
	Processor and handlers - activity diagram & Konrad Gancarz \\ \hline
	\makecell[l]{Kendal AST representation - design \& implementation\\ (classes: \lstinline|Node|,
		\lstinline|ForestBuilder|, \lstinline|TreeBuilder|)} & Konrad Gancarz, Arkadiusz Ryszewski \\ \hline
	Kendal AST representation - diagrams & Arkadiusz Ryszewski \\ \hline
	Kendal API - design & Konrad Gancarz, Arkadiusz Ryszewski \\ \hline
	Kendal API - \lstinline|AstHelper| implementation & Konrad Gancarz, Arkadiusz Ryszewski	\\ \hline
	\makecell[l]{Kendal API - \lstinline|AstNodeBuilder| \& \lstinline|AstValidator|\\ implementation} & Arkadiusz Ryszewski \\ \hline
	TypeScript Fields feature implementation & Konrad Gancarz, Arkadiusz Ryszewski \\ \hline
	Clone feature implementation & Arkadiusz Ryszewski \\ \hline
	String Interpolation feature implementation & Konrad Gancarz \\ \hline
	\makecell[l]{Indirect Annotations feature implementation\\ (finished but abandoned)} & Arkadiusz Ryszewski \\ \hline
	\makecell[l]{Annotation Inheritance feature implementation\\ (\lstinline|@Inherit|, \lstinline|@Attribute|, 				
			\lstinline|@AttrReference|)}
					& Konrad Gancarz \\ \hline
	Tests - Proof of Concept (with \lstinline|jtreg|) & Konrad Gancarz \\ \hline
	Tests - implementation (with \lstinline|jtreg|) & Konrad Gancarz, Arkadiusz Ryszewski \\ \hline
	Manual (HTML page) & Arkadiusz Ryszewski \\ \hline
	README file & Konrad Gancarz, Arkadiusz Ryszewski \\ \hline
	Publishing project to Maven Repository & Konrad Gancarz \\ \hline
\end{longtable}

\subsection{Plugin}
\begin{longtable}{|p{12.4cm} | p{3.5cm}|}
	\hline
	\textbf{Element} & \textbf{Author(s)} \\ \hline
	JetBrains Platform Plugins - research & Arkadiusz Ryszewski \\ \hline
	JetBrains Platform Plugins - Proof of Concept & Arkadiusz Ryszewski \\ \hline
	\makecell[l]{TypeScript Fields feature - errors suppression\\ (80\% finished but abandoned)} & Arkadiusz Ryszewski \\ \hline
	TypeScript Fields feature support (\lstinline|TsFieldsAugmentProvider| class) & Konrad Gancarz \\ \hline
	\makecell[l]{Clone feature support (classes: \\ \lstinline|CloneAugmentProvider|, \lstinline|CloneOnMethodAugmentProvider|)}
					& Konrad Gancarz \\ \hline
	\makecell[l]{Clone feature - Indirect Annotations support (finished but abandoned)\\ (\lstinline|CloneAugmentProvider| class)}
					& Arkadiusz Ryszewski \\ \hline
	\makecell[l]{String Interpolation feature support\\ (\lstinline|StringInterpolationHighlightInfoFilter| class)}
					& Arkadiusz Ryszewski \\ \hline
	\makecell[l]{Inherit feature support (classes:\\ \lstinline|InheritAugmentProvider|,
			\lstinline|InheritValueAugmentProvider|, \\ \lstinline|InheritMissingRequiredAttributeHighlightInfoFilter|)} 
					& Konrad Gancarz \\ \hline
	\makecell[l]{Attribute feature support (classes: \lstinline|AttributeValueAugmentProvider|,\\
			\lstinline|AttributeValueHighlightInfoFilter|)}
					& Konrad Gancarz \\ \hline
	\makecell[l]{AttrReference feature support\\ (\lstinline|AttrReferenceHighlightInfoFilter| class)}
					& Konrad Gancarz \\ \hline
	Acceptance tests scenarios & Arkadiusz Ryszewski \\ \hline 
	README file & Arkadiusz Ryszewski \\ \hline
	Publishing project to JetBrains Plugin Repository & Arkadiusz Ryszewski \\ \hline
\end{longtable}


\subsection{Thesis Paper} 
\begin{longtable}{|p{9.4cm} | p{6.5cm}|}
	\hline
	\textbf{Element} & \textbf{Author(s)} \\ \hline
	\makecell[l]{Initial document preparation based on official template} & Arkadiusz Ryszewski \\ \hline
	Title Page preparation based on official template & Arkadiusz Ryszewski \\ \hline
	Abstract & Konrad Gancarz \\ \hline
	Streszczenie (Abstract in polish) & Konrad Gancarz \\ \hline
	Business Analysis - 1.1.1 \backgroundTitle{} & Arkadiusz Ryszewski \\ \hline
	Business Analysis - 1.1.1 \problemTitle{} & Arkadiusz Ryszewski \\ \hline
	Business Analysis - 1.1.3 \desiredSolutionTitle{} & Konrad Gancarz \\ \hline
	Business Analysis - 1.1.4 \inspirationTitle{} & Konrad Gancarz, Arkadiusz Ryszewski \\ \hline
	Business Analysis - 1.1.5 \documentStructureTitle{} & Konrad Gancarz \\ \hline
	Business Analysis - 1.2 \caseStudyTitle{} & Konrad Gancarz, Arkadiusz Ryszewski \\ \hline
	Business Analysis - 1.3.1 \descriptionOfProductTitle{} & Arkadiusz Ryszewski \\ \hline
	Business Analysis - 1.3.2 \howItCanBeUsedTitle{} & Arkadiusz Ryszewski \\ \hline
	Business Analysis - 1.3.3 \howItWorksTitle{} & Konrad Gancarz, Arkadiusz Ryszewski \\ \hline
	Business Analysis - 1.4 \functionalRequirementsTitle{} & Konrad Gancarz, Arkadiusz Ryszewski \\ \hline
	Business Analysis - 1.5 \nonFunctionalRequirementsTitle{} & Arkadiusz Ryszewski \\ \hline
	Business Analysis - 1.6 \riskAnalysisAndManagementTitle{} & Arkadiusz Ryszewski \\ \hline
	Business Analysis - 1.7 \workScheduleTitle{} & Konrad Gancarz \\ \hline
	Technical Analysis - Introduction & Arkadiusz Ryszewski \\ \hline
	Technical Analysis - 2.1 \javaCompilersTitle{}: Introduction & Arkadiusz Ryszewski \\ \hline
	Technical Analysis - 2.1.1 \compilationProcessInJavaTitle{} & Konrad Gancarz \\ \hline
	Technical Analysis - 2.1.2 \annotationProcessingInJavaTitle{} & Arkadiusz Ryszeski \\ \hline
	Technical Analysis - 2.1.3 \hackingCompilationProcessTitle{} & Konrad Gancarz, Arkadiusz Ryszeski \\ \hline
	\makecell[l]{Technical Analysis - \\ 2.2.1 \architectureOfTheSolutionTitle{}: \processorModuleTitle{}} 
					& 	\makecell[l]{Konrad Gancarz, Arkadiusz Ryszeski} \\ \hline
	\makecell[l]{Technical Analysis - \\ 2.2.1 \architectureOfTheSolutionTitle{}: \handlersModuleTitle{}} 
					& \makecell[l]{Konrad Gancarz} \\ \hline
	Technical Analysis - 2.2.2 \testingTitle{} & Konrad Gancarz \\ \hline
	Technical Analysis - 2.2.3 \kendalApiTitle{} & Arkadiusz Ryszewski \\ \hline
	\makecell[l]{Technical Analysis - \\ 2.3 \kendalPluginForIntellijIdeaTitle{}: Introduction} 
					& \makecell[l]{Arkadiusz Ryszewski} \\ \hline
	Technical Analysis - 2.3.1 \pluginStructureTitle{} & Arkadiusz Ryszewski \\ \hline
	Technical Analysis - 2.3.2 \ourSolutionTitle{} & Arkadiusz Ryszewski \\ \hline
	Technical Analysis - 2.3.3 \testingTitle{} & Arkadiusz Ryszewski \\ \hline
	Technical Analysis - 3.1.1 \typescriptFieldsTitle{} & Arkadiusz Ryszewski \\ \hline
	Technical Analysis - 3.1.2 \stringInterpolationTitle{} & Konrad Gancarz \\ \hline
	Technical Analysis - 3.1.3 \cloneTitle{} & Arkadiusz Ryszewski \\ \hline
	Technical Analysis - 3.1.4 \inheritTitle{} & Konrad Gancarz \\ \hline
	Technical Analysis - 3.1.5 \attributeTitle{} & Konrad Gancarz, Arkadiusz Ryszewski \\ \hline
	Technical Analysis - 3.1.6 \attrReferenceTitle{} & Konrad Gancarz \\ \hline
	Technical Analysis - 3.2.1 \frameworkTitle{}: \virtualAttributesTitle{} & Konrad Gancarz \\ \hline
	Technical Analysis - 3.2.2 \pluginTitle{}: \kotlinTitle{} & Arkadiusz Ryszewski \\ \hline
	Technical Analysis - 3.3 \distributionTitle{} & Konrad Gancarz, Arkadiusz Ryszewski \\ \hline
	\makecell[l]{Technical Analysis - 3.4.1 \\ \inheritanceModelOfAnnotationsTitle{}} & Konrad Gancarz \\ \hline
	\makecell[l]{Technical Analysis - 3.4.2 \\ \findingHandledAnnotationsInAstForestTitle{}} & \makecell[l]{Konrad Gancarz} \\ \hline
	Technical Analysis - 3.4.3 \handlersWithoutAnnotationsTitle{} & Konrad Gancarz \\ \hline
	\makecell[l]{Technical Analysis - 3.4.4 \\ \moreGeneralApproachToFunctionalitiesOfThePluginTitle{}} 
					& \makecell[l]{Arkadiusz Ryszewski} \\ \hline
	Technical Analysis - 3.5.1 \frameworkTitle{} & Konrad Gancarz \\ \hline
	Technical Analysis - 3.5.2 \pluginTitle{} & Arkadiusz Ryszewski \\ \hline
	Technical Analysis - 4.1.1 \newHandlersTitle{} & Konrad Gancarz \\ \hline
	Technical Analysis - 4.1.2 \compilersSupportTitle{} & Konrad Gancarz \\ \hline
	Technical Analysis - 4.1.3 \javaSupportTitle{} & Konrad Gancarz \\ \hline
	Technical Analysis - 4.1.4 \pluginSupportTitle{} & Konrad Gancarz \\ \hline
	Technical Analysis - 4.2 \distributionOfWorkTitle{} & Arkadiusz Ryszewski \\ \hline
	A. User Manual & Arkadiusz Ryszewski \\ \hline
	B. Glossary & Konrad Gancarz, Arkadiusz Ryszewski \\ \hline
	C. List of Abbreviations & Konrad Gancarz, Arkadiusz Ryszewski \\ \hline
	D. Diagrams & Arkadiusz Ryszewski \\ \hline
\end{longtable}

\newcommand{\listOfFiguresTitle}{List of Figures}
\section{\listOfFiguresTitle{}}
\begin{longtable}{|p{9.4cm} | p{6.5cm}|}
	\hline
	\textbf{Table} & \textbf{Page number} \\ \hline
	Figure 1.1: Grantt Chart - Schedule & 12 \\ \hline
	Figure 2.1: javac compilation process sequence diagram & 17 \\ \hline
	Figure 2.2: AST representation in javac compiler & 19 \\ \hline
	Figure 2.3: Possible children types of selected types of Kendal nodes & 22 \\ \hline
	Figure 2.4: Kendal processor \& handlers activity diagram & 23 \\ \hline
\end{longtable}

\newcommand{\listOfTablesTitle}{List of Tables}
\section{\listOfTablesTitle{}}
\begin{longtable}{|p{9.4cm} | p{6.5cm}|}
	\hline
	\textbf{Table} & \textbf{Page number} \\ \hline
	Risk Matrix & 9 \\ \hline % had to hard-code this because referencing mechanism does not work here... :/
	Distribution of work - Framework & 43 \\ \hline
	Distribution of work - Plugin & 44 \\ \hline
	Distribution of work - Thesis Paper & 44 \\ \hline
	\listOfFiguresTitle{} & 46 \\ \hline
	\listOfTablesTitle{} & 46 \\ \hline
\end{longtable}