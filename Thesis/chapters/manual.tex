\section{\projectName{} Framework}

\subsection{How to Add Framework to My Project?}
\subsubsection{Maven Project (Recommended)}
In order to start using \projectName{} framework in your Maven project it is enough to add 2 dependencies in project's pom.xml file:
\begin{lstlisting}[language=Xml]
<dependencies>
	<dependency>
		<groupId>com.github.projectkendal</groupId>
		<artifactId>handlers</artifactId>
		<version>LATEST_KENDAL_VERSION</version>
		<scope>compile</scope>
	</dependency>
	<dependency>
		<groupId>com.github.projectkendal</groupId>
		<artifactId>processor</artifactId>
		<version>LATEST_KENDAL_VERSION</version>
	</dependency>
</dependencies>
\end{lstlisting}
Remember to replace LATEST\_KENDAL\_VERSION with the latest Kendal version that is available.
You can check it for example here: \url{https://mvnrepository.com/artifact/com.github.projectkendal/Kendal}

\subsubsection{Not Maven Project (Not Recommended)}
Download "processor" and "handlers" jar files from \url{https://mvnrepository.com/artifact/com.github.projectkendal}
and add them to your project:

\begin{enumerate}
	\item Click "File" from the toolbar
	\item Project Structure (CTRL + SHIFT + ALT + S on Windows/Linux)
	\item Select Modules at the left panel
	\item Dependencies tab
	\item '+' -> JARs or directories
\end{enumerate}

\subsection{How to Use Framework's Features?}
We present short description and examples of how features of \projectName{} framework can be used in subsequent sections.

\subsection{TypeScript Fields}
Main inspiration for that feature have been dependency injection frameworks. Preferred way to inject fields'
values to classes is to do it via constructor. This forces very repetitive construction of constructors where
we always have argument passed and assigned to the corresponding field in a class. TypeScript Fields feature
is designed to let programmer get rid of that redundancy with possibility of automatic fields' generation.
There are 4 annotations that contribute to that feature: \lstinline|@Private|, \lstinline|@PackagePrivate|,
\lstinline|@Protected| and \lstinline|@Public|. Each of it should be applied to the constructor in a class
and causes Kendal to generate field with proper access modifier. There is also an optional attribute
\lstinline|makeFinal| which let's programmer decide whether new field should be final or not. It defaults to \lstinline|true|.
\subsubsection{Vanilla Java}
\begin{lstlisting}[language=Java]
@Service
class FooService {

	private final SomeFactory someFactory;
	private final SpecialValidator specialValidator;
	private final ItemRepository itemRepository;
	
	@Autowired
	FooService(SomeFactory someFactory, SpecialValidator specialValidator, ItemRepository itemRepository) {
		this.someFactory = someFactory;
		this.specialValidator = specialValidator;
		this.itemRepository = itemRepository;
		
		// some logic
	}
	
	Object createNewItem(Object newItemDto) {
		specialValidator.validate(newItemDto);
		Object newItem = someFactory.construct(newItemDto);
		return itemRepository.save(newItem);
	}
}
\end{lstlisting}
\subsubsection{Kendal}
\begin{lstlisting}[language=Java]
@Service
class FooService {

	@Autowired
	FooService(@Private SomeFactory someFactory, @Private SpecialValidator specialValidator,
	@Private ItemRepository itemRepository) {
		// some logic
	}
	
	Object createNewItem(Object newItemDto) {
		specialValidator.validate(newItemDto);
		Object newItem = someFactory.construct(newItemDto);
		return itemRepository.save(newItem);
	}
}
\end{lstlisting}


\subsection{String Interpolation}
To use this mechanism is enough to put "+" operator in front of a string literal. Then it is possible to use any valid
Java expressions inside of the string using \lstinline|${expression}| syntax. See example below for details.
\subsubsection{Vanilla Java}
\begin{lstlisting}[language=Java]
class SomeClass {

	SomeFactory someFactory;
	
	// [...]
	
	String generateName1(String component, int postfix) {
		return "specificPrefix " + component + " (" + someFactory.uniqueIdentifier() + ") " + postfix;
	}
	
	String generateName2(String component, int postfix) {
		return String.format("specificPrefix %s (%s) %d", component, someFactory.uniqueIdentifier(), postfix);
	}
	
	String generateName3(String component, int postfix) {
		return new StringBuilder("specialPrefix ").append(component).append(" (").append(someFactory.uniqueIdentifier()).append(") ").append(postfix).toString();
	}
}
\end{lstlisting}
\subsubsection{Kendal}
\begin{lstlisting}[language=Java]
class SomeClass {

	SomeFactory someFactory;
	
	// [...]
	
	String generateNameKendal(String component, int postfix) {
		return +"specificPrefix ${component} (${someFactory.uniqueIdentifier()}) ${postfix}";
	}
}
\end{lstlisting}


\subsection{Clone}
Applied to a method creates new method with name from \lstinline|methodName| attribute and list of parameters same as 
original method. Generated method calls the original method, passes the result to an instance of transformer specified 
with \lstinline|transformer| attribute and returns transformed value. If \lstinline|methodName| is not specified, clone 
with default name is created. Default name is: \lstinline|originalName + "Clone"|. Return type of generated method is 
extracted from declaration of the transformer class.
\subsubsection{Vanilla Java}
\begin{lstlisting}[language=Java]
class SomeClass {

	class CsvTransformer {
		String transform(List<Object> inputCollection) {
			return "imagine here is the original collection serialized to csv";
		}
	}
	
	List<Object> method1(int oneParam, String anotherParam) {
		List<Object> result = new ArrayList();
		// some logic
		return result;
	}
	
	List<Object> method2(int oneParam, String anotherParam) {
		List<Object> result = new ArrayList();
		// some logic
		return result;
	}
	
	String method1Csv(int oneParam, String anotherParam) {
		return new CsvTransformer().transform(method1(oneParam, anotherParam));
	}
	
	String method2Csv(int oneParam, String anotherParam) {
		return new CsvTransformer().transform(method2(oneParam, anotherParam));
	}
}
\end{lstlisting}
\subsubsection{Kendal}
\begin{lstlisting}[language=Java]
class SomeClass {

	class CsvTransformer implements Clone.Transformer<List<Object>, String> {
		@Override
		public String transform(List<Object> inputCollection) {
			return "imagine here is the original collection serialized to csv";
		}
	}
	
	@Clone(transformer = CsvTransformer.class, methodName="method1Csv")
	List<Object> method1(int oneParam, String anotherParam) {
		List<Object> result = new ArrayList();
		// some logic
		return result;
	}
	
	@Clone(transformer = CsvTransformer.class, methodName="method2Csv")
	List<Object> method2(int oneParam, String anotherParam) {
		List<Object> result = new ArrayList();
		// some logic
		return result;
	}
}
\end{lstlisting}


\subsection{Inherit, Attribute \& AttReference}
\subsubsection{@Inherit}
\begin{enumerate}
	\item This annotation causes all parameters from value attribute to be inherited. Remember that value must be an annotation.
	\item Kendal handlers handling value will receive also annotations that inherit parameters from value using @Inherit.
\end{enumerate}
Note that this relation is transitive.

\subsubsection{@Attribute}
This annotation will add named parameter to all usages of annotated annotation. Use @Attribute.List to add multiple parameters. Parameters will be added to usages of annotation before handlers are called.
Use @AttrReference as placeholder for values of any other attributes.

\subsubsection{@AttrReference}
Java does not allow using parameters of annotation to define other parameters of the same annotation. But sometimes we want to, so here we introduce annotation that will be replaced with value of another parameter.
It can be used only inside value expression for @Attribute. Some expressions using @AttrReference will cause the compiler to break compilation before annotation processing - in such case @AttrReference will never work. This feature is currently considered a proof of concept showing that it is possible to access other attributes of annotation to define value of attribute.

\subsubsection{Example With Clone:}
\subsubsection{Vanilla Java}
\begin{lstlisting}[language=Java]
public class SomeClass {

	class CsvTransformer {
		String transform(List<Object> inputCollection) {
			return "imagine here is the original collection serialized to csv";
		}
	}
	
	@RequestMapping(value = "/method1", method = "POST")
	public List<Object> method1(RequestBody body) {
		List<Object> result = new ArrayList();
		// some logic
		return result;
	}
	
	@RequestMapping(value = "/method2", method = "POST")
	public List<Object> method2(RequestBody body) {
		List<Object> result = new ArrayList();
		// some logic
		return result;
	}
	
	@AnotherAnnotation
	@RequestMapping(value = "/method1/csv", method = "POST")
	public String method1Csv(RequestBody body) {
		List<Object> result = new ArrayList();
		// some logic
		return new CsvTransformer().transform(result);
	}
	
	@AnotherAnnotation
	@RequestMapping(value = "/method2/csv", method = "POST")
	public String method2Csv(RequestBody body) {
		List<Object> result = new ArrayList();
		// some logic
		return new CsvTransformer().transform(result);
	}
	
	@interface AnotherAnnotation {}
}
\end{lstlisting}

\subsubsection{Kendal Without @Inherit}
\textcolor{red}{Impossible!}

\subsubsection{Kendal With @Inherit}
\begin{lstlisting}[language=Java]
public class SomeClass {

	@Inherit(@Clone(transformer = CsvTransformer.class))
	@Attribute(name = "onMethod", value = {@RequestMapping(value = @AttrReference("endpoint"), method = "POST"),
											@AnotherAnnotation})
	@interface CsvEndpoint {
		String endpoint();
	}
	
	class CsvTransformer implements Clone.Transformer<List<Object>, String> {
		@Override
		public String transform(List<Object> inputCollection) {
			return "imagine here is the original collection serialized to csv";
		}
	}
	
	@CsvEndpoint(endpoint = "method1/csv", methodName = "method1Csv")
	@RequestMapping(value = "/method1", method = "POST")
	public List<Object> method1(RequestBody body) {
		List<Object> result = new ArrayList();
		// some logic
		return result;
	}
	
	@CsvEndpoint(endpoint = "method2/csv", methodName = "method2Csv")
	@RequestMapping(value = "/method2", method = "POST")
	public List<Object> method2(RequestBody body) {
		List<Object> result = new ArrayList();
		// some logic
		return result;
	}
	
	@interface AnotherAnnotation {}
}
\end{lstlisting}

\subsubsection{Example With TypeScript Fields:}
\subsubsection{Vanilla Java}
\begin{lstlisting}[language=Java]
class SomeClass {

	int primitiveField;
	
	SomeClass(int primitiveField) {
		this.primitiveField = primitiveField;
	}
	
	private int aMethod() {
		return this.primitiveField + 15;
	}
}
\end{lstlisting}
\subsubsection{Kendal Without @Inherit}
\begin{lstlisting}[language=Java]
class SomeClass {

	SomeClass(@Private(makeFinal = false) int primitiveField) { }
	
	private int aMethod() {
		return this.primitiveField + 15;
	}
}
\end{lstlisting}
\subsubsection{Kendal With @Inherit}
\begin{lstlisting}[language=Java]
class SomeClass {

	SomeClass(@PrivateNotFinal int primitiveField) { }
	
	private int aMethod() {
		return this.primitiveField + 15;
	}
	
	@Inherit(@Private(makeFinal = false))
	@Target(ElementType.PARAMETER)
	@interface PrivateNotFinal { }
}
\end{lstlisting}

\newpage
\section{\projectName{} API}
From the very beginning Kendal was designed to be open for new extensions. Here we present a short guide how to make your own custom functionality.

\subsection{Create Project}
First thing that has to be done is obviously project creation. Although Kendal works in any kind of Java project, and using any
build system, for simplicity reasons we are going to create a Maven project. This project will contain handler implementation
and annotation definition. It will be also responsible for handler registration to Kendal.

\subsection{Add Dependencies}
After project is created we have to add dependencies to it. In this manual we assume that we work with projects managed by 
Maven. In such case, it is very easy to add required dependencies. Just add this snipped to your \texttt{pom.xml} file:

\begin{lstlisting}[language=Xml]
	<dependencies>
		<dependency>
			<groupId>com.github.projectkendal</groupId>
			<artifactId>processor</artifactId>
			<version>LATEST_KENDAL_VERSION</version>
		</dependency>
	</dependencies>
\end{lstlisting}

Remember to replace LATEST\_KENDAL\_VERSION with the latest Kendal version that is available. 
You can check it for example here: \url{https://mvnrepository.com/artifact/com.github.projectkendal/Kendal}

\subsection{Disable Annotation Processing}
To make SPI mechanism work correctly we have to disable annotation processing in our handler's project. It is usually done by adding 
\lstinline|-proc:none| parameter when executing the compiler. It Maven it can be achieved by adding the following code to the 
\texttt{pom.xml} file:

\begin{lstlisting}[language=Xml]
	<build>
		<plugins>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-compiler-plugin</artifactId>
				<version>3.7.0</version>
				<configuration>
					<source>1.8</source>
					<target>1.8</target>
					<!-- Disable annotation processing -->
					<compilerArgument>-proc:none</compilerArgument>
				</configuration>
			</plugin>
		</plugins>
	</build>
\end{lstlisting}

\subsection{Create Handler}
Whatever logic you may want to implement, you must attach it to the Kendal processing process. First step to do it is to create a 
class implementing\lstinline| kendal.api.KendalHandler<T extends Annotation>| interface. This interface defines one method: 
\lstinline|void handle(Collection<Node> handledNodes, AstHelper helper)|. This method is called by processor for each registered 
handler. First parameter collection of nodes representing annotations. In runtime, that collection will contain all occurrences of 
annotations (or inheriting annotations) of type with which handler is connected via type parameter \lstinline|<T extends Annotation>| 
declaration.

\subsubsection{Example}
Let's assume we want to create a handler for our awesome feature. This feature will rely on annotation \lstinline|@Awesome|, that 
will be allowed to be put on classes declarations. This way, programmer will claim that he/she wants this class to be automatically 
refactored to be awesome. First step to achieve this is to create annotation and its corresponding handler:

\begin{lstlisting}[language=Java]
@Target(ElementType.TYPE)
public @interface Awesome { }

public class AwesomeHandler implements KendalHandler<Awesome> {
	
	@Override
	public void handle(Collection<Node> handledNodes, AstHelper helper) {
		// some logic
	}
}
\end{lstlisting}

\subsection{Register Handler}
Next step is to let Kendal know about your handler. You can do it using SPI mechanism. That is quite simple. It is enough to create 
in your project's \texttt{resources} directory a file: \texttt{META-INF/services/kendal.api.KendalHandler}. This file should contain 
a list of handlers implemented in the project, each in the new line.

\subsubsection{Example}
In the case of our example project, this file's content will be very simple:
\begin{lstlisting}
kendal.apiExperiments.handler.AwesomeHandler
\end{lstlisting}
After this is done, our project is fully functional Kendal handler.

\subsection{Utilizing AstHelper}
Now we can start implementing our awesome feature. As you already learned, method \lstinline|handle| is the core of a handler. We 
know that first parameter is a collection of annotation nodes. Now it is time to learn about the second parameter - 
\lstinline|AstHelper helper|. This is a very useful class, it aims at facilitating whatever you may want to do to AST tree. It 
delivers methods that allow AST nodes modification. It also shares some more specific helpers: \lstinline|AstNodeBuilder|, 
\lstinline|AstValidator| and \lstinline|AstUtils|. They contain methods which are often needed in projects of this kind. Using them 
saves your time and lines of code.

\subsection{Using Own Handler}
After we finish our handler, we can start using it in other projects. In order to do this, we have to add some dependencies to the 
target project. These are \lstinline|com.github.projectkendal.processor| and your own project.

\subsubsection{Example}
If we want to add our custom handler to some other application we have to add the following code to the pom.xml file of that
application:

\begin{lstlisting}[language=Xml]
	<dependencies>
		<dependency>
			<groupId>com.github.projectkendal</groupId>
			<artifactId>processor</artifactId>
			<version>LATEST_KENDAL_VERSION</version>
		</dependency>
		<dependency>
			<groupId>your.package</groupId>
			<artifactId>awesome</artifactId>
			<version>AWESOME_HANDLER_VERSION</version>
		</dependency>
	</dependencies>
\end{lstlisting}

Then we can start using our annotation:

\begin{lstlisting}[language=Java]
@Awesome
public class SomeClass {
	// logic
}
\end{lstlisting}

Our \lstinline|AwesomeHandler| will be run whenever there is at least one usage of \lstinline|@Awesome| annotation in the target 
project. Each such usage will be included in \lstinline|handledNodes| collection that is passed to \lstinline|handle| method of our
handler.


\section{\projectName{} Plugin}

\subsection{How to Add Plugin to IntelliJ IDEA?}
\begin{enumerate}
	\item Click "File"
	\item Click "Settings"
	\item Select "Plugins" tab
	\item Click "Browse repositories..." button
	\item Search for "Kendal"
	\item Click "Install"
	\item Restart IntelliJ IDEA
\end{enumerate}